#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, extract_movie_id, calculate_rmse
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.1\n2.9\n3.6\n0.78\n")

    def test_eval_2(self):
        r = StringIO("10041:\n10043:\n852886\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10041:\n10043:\n4.2\n0.79\n")

    def test_eval_3(self):
        r = StringIO("10041:\n869522\n10042:\n449288\n10043:\n852886\n"
                     "2204494\n774717\n1406050\n2317868\n202667\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10041:\n3.4\n10042:\n4.3\n10043:\n4.2\n"
                          "4.1\n3.6\n4.1\n4.0\n3.7\n0.90\n")

    def test_extract_id_1(self):
        self.assertTrue(
            extract_movie_id("10036:") == 10036)

    def test_extract_id_2(self):
        self.assertTrue(
            extract_movie_id("0:") == 0)

    def test_extract_id_3(self):
        self.assertRaises(ValueError, extract_movie_id, "NaN:")

    def test_extract_id_4(self):
        self.assertRaises(ValueError, extract_movie_id, "None:")

    def test_extract_id_5(self):
        self.assertRaises(ValueError, extract_movie_id, ":")

    def test_extract_id_6(self):
        self.assertTrue(extract_movie_id("0::") == 0)

    def test_get_RMSE_1(self):
        self.assertTrue(calculate_rmse([1, 2, 3, 4],
                                       [2, 3, 4, 5]) == "1.0")

    def test_get_RMSE_2(self):
        self.assertRaises(AssertionError, calculate_rmse,
                          [], [])

    def test_get_RMSE_3(self):
        self.assertRaises(AssertionError, calculate_rmse,
                          [1], [1, 1])

    def test_get_RMSE_4(self):
        self.assertTrue(calculate_rmse([2.4, 2.4, 2.4],
                                       [1, 2, 3]) == "0.90")

    def test_get_RMSE_5(self):
        self.assertRaises(AssertionError, calculate_rmse,
                          [0, 0, 0, 0, 0], [2, 2, 2, 2, 2])


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
